<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Usuarios
         */
        Permission::create([
            'name' => 'Navegar usuarios',
            'slug' => 'users.index',
            'description' => 'Lista y navega todos los usuarios del sistema',
        ]);

        Permission::create([
            'name' => 'Ver detalle usuario',
            'slug' => 'users.show',
            'description' => 'Ver en detalle cada usuario del sistema',
        ]);

        Permission::create([
            'name' => 'Edicion usuarios',
            'slug' => 'users.edit',
            'description' => 'Editar cualquier dato de un usuario del sistema',
        ]);

        Permission::create([
            'name' => 'Eliminar usuarios',
            'slug' => 'users.destroy',
            'description' => 'Eliminar cualquier usuario del sistema',
        ]);

        /**
         * Roles
         */
        Permission::create([
            'name' => 'Navegar roles',
            'slug' => 'roles.index',
            'description' => 'Lista y navega todos los roles del sistema',
        ]);

        Permission::create([
            'name' => 'Ver detalle rol',
            'slug' => 'roles.show',
            'description' => 'Ver en detalle cada rol del sistema',
        ]);

        Permission::create([
            'name' => 'Edicion roles',
            'slug' => 'roles.edit',
            'description' => 'Editar cualquier dato de un rol del sistema',
        ]);

        Permission::create([
            'name' => 'Eliminar roles',
            'slug' => 'roles.destroy',
            'description' => 'Eliminar cualquier rol del sistema',
        ]);

        Permission::create([
            'name' => 'Creacion roles',
            'slug' => 'roles.create',
            'description' => 'Eliminar cualquier rol del sistema',
        ]);

        /**
         * Productos
         */
        Permission::create([
            'name' => 'Navegar productos',
            'slug' => 'products.index',
            'description' => 'Lista y navega todos los productos del sistema',
        ]);

        Permission::create([
            'name' => 'Ver detalle producto',
            'slug' => 'products.show',
            'description' => 'Ver en detalle cada producto del sistema',
        ]);

        Permission::create([
            'name' => 'Edicion productos',
            'slug' => 'products.edit',
            'description' => 'Editar cualquier dato de un producto del sistema',
        ]);

        Permission::create([
            'name' => 'Eliminar productos',
            'slug' => 'products.destroy',
            'description' => 'Eliminar cualquier producto del sistema',
        ]);

        Permission::create([
            'name' => 'Creacion productos',
            'slug' => 'products.create',
            'description' => 'Eliminar cualquier producto del sistema',
        ]);
    }
}
