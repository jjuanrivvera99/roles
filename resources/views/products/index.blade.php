@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Productos</div>
                @can('products.create')
                    <a class="btn btn-success pull-right" href="{{ route('products.create') }}">Crear</a>
                @endcan
                
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3">&nbsp</th>
                            </tr>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->id}}</td>
                                    <td>{{ $product->name}}</td>
                                    <td>
                                        @can('products.show')
                                        <a class="btn btn-sm btn-warning" href="{{route('products.show', $product->id)}}">Ver</a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('products.edit')
                                        <a class="btn btn-sm btn-primary" href="{{route('products.edit', $product->id)}}">Editar</a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('products.destroy')
                                            {!! Form::open(['route' => ['products.destroy', $product->id],
                                            'method' => 'DELETE']) !!}
                                            <button class="btn btn-sm btn-danger">
                                                Eliminar
                                            </button>

                                            {!! Form::close() !!}
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </thead>
                    </table>
                    {{ $products->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
