@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Roles</div>
                @can('roles.create')
                    <a class="btn btn-success pull-right" href="{{ route('roles.create') }}">Crear</a>
                @endcan
                
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3">&nbsp</th>
                            </tr>
                            <tbody>
                                @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id}}</td>
                                    <td>{{ $role->name}}</td>
                                    <td>
                                        @can('roles.show')
                                        <a class="btn btn-sm btn-warning" href="{{route('roles.show', $role->id)}}">Ver</a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('roles.edit')
                                        <a class="btn btn-sm btn-primary" href="{{route('roles.edit', $role->id)}}">Editar</a>
                                        @endcan
                                    </td>
                                    <td>
                                        @can('roles.destroy')
                                            {!! Form::open(['route' => ['roles.destroy', $role->id],
                                            'method' => 'DELETE']) !!}
                                            <button class="btn btn-sm btn-danger">
                                                Eliminar
                                            </button>

                                            {!! Form::close() !!}
                                        @endcan
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </thead>
                    </table>
                    {{ $roles->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
